//
//  GalleryViewController.m
//  ArtNewsApp
//
//  Created by iMac on 24.09.15.
//  Copyright © 2015 Boost. All rights reserved.
//

#import "CategoryViewController.h"
#import "GalleryViewController.h"
#import "SearchViewController.h"
#import "DownloadsManager.h"
#import "CategoryCell.h"
#import "ArtCategory.h"

#define kWidthCell (self.view.frame.size.width - 30) / 2
#define kLimitDownloads 20

static NSString *const kCategoryCell = @"CategoryCell";

@interface CategoryViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    int currentPosition;
    BOOL isUpdate;
}

@property (strong, nonatomic) NSArray *categoryArray;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CategoryViewController
#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setCartButton:YES searcheButton:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self downloadCategory];
}

#pragma mark - Searche
- (void)searcheAction
{
    SearchViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    svc.modalPresentationStyle = UIModalPresentationCustom;
    svc.modalPresentationCapturesStatusBarAppearance = YES;
    [svc setNeedsStatusBarAppearanceUpdate];
    [svc setupSearcheWithType:SearchTypeCategory root:self];
    [self presentViewController:svc animated:YES completion:nil];
}

#pragma mark - download items
- (void)downloadCategory
{
    [self startAnimation];
    
    [[DownloadsManager sharedInstance] getCategoryWithCallBack:^(RequestResult *requestResult) {
        self.categoryArray = [[NSArray alloc] initWithArray:requestResult.resultArray];
        [self.collectionView reloadData];
        
        [self stopAnimation];
    }];
}


#pragma mark - collection view metods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.categoryArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCategoryCell forIndexPath:indexPath];
    ArtCategory *item = self.categoryArray[indexPath.row];
    [cell configureCellWithCategory:item];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kWidthCell, kWidthCell);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ArtCategory *category = self.categoryArray[indexPath.row];
    GalleryViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GalleryViewController"];
    [vc setCategory:category];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
