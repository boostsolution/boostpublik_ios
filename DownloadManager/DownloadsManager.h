//
//  DownloadManager.h
//  ArtNewsApp
//
//  Created by iMac on 11.09.15.
//  Copyright © 2015 Boost. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "RequestResult.h"
#import "User.h"


typedef void (^requestCallBack)(RequestResult *requestResult);


@interface DownloadsManager : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isAutentificate;
- (NSString *)currentUserID;
- (BOOL)isAuthor;
- (void)setUserIcon:(UIImage *)icon;
- (UIImage *)getUserIcon;

//User
- (void)createNewUser:(User *)user callBack:(requestCallBack)callBack;
- (void)updateUserWithData:(NSDictionary *)userData callBack:(requestCallBack)callBack;
- (void)autentificationWith:(NSString *)login password:(NSString *)password callBack:(requestCallBack)callBack;
- (void)getUserInfoWithCallBack:(requestCallBack)callBack;
- (void)confirmUserEmail:(NSString *)email privateKey:(NSString *)key callBack:(requestCallBack)callBack;
- (void)resetPasswordWithEmail:(NSString *)email callBack:(requestCallBack)callBack;
- (void)setNewPassword:(NSString *)password email:(NSString *)email recovery:(NSString *)recoveryKey callBack:(requestCallBack)callBack;
//Artworks
- (void)getArtworksWithStartIndex:(int)startIndex limit:(int)limit sortMode:(BOOL)mode withCallBack:(requestCallBack)callBack;
- (void)getArtworksWithAutor:(NSString *)author startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack;
- (void)getArtworksWithCategory:(NSString *)category startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack;
- (void)getArtworkWithID:(NSString *)artworkID withCallBack:(requestCallBack)callBack;
- (void)getAllLikesArtworkStartIndex:(NSInteger)start limit:(NSInteger)limit withCallBack:(requestCallBack)callBack;
//Authors
- (void)getAuthorsWithMode:(BOOL)mode startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack;
- (void)getAuthorsWithID:(NSString *)authorID withCallBack:(requestCallBack)callBack;
- (void)becomeAuthorWithEmail:(NSString *)email fullName:(NSString *)fullName withCallBack:(requestCallBack)callBack;
- (void)followAuthor:(NSString *)autorID withCallBack:(requestCallBack)callBack;
- (void)unfollowAuthor:(NSString *)autorID withCallBack:(requestCallBack)callBack;
//Category
- (void)getCategoryWithCallBack:(requestCallBack)callBack;
//Payments list
- (void)getPaymentsListWithCallBack:(requestCallBack)callBack;
//Likes
- (void)likeArtwork:(NSString *)workID withCallBack:(requestCallBack)callBack;
- (void)unLikeArtwork:(NSString *)workID withCallBack:(requestCallBack)callBack;
//Cart List
- (void)getCartListStartIndex:(NSInteger)startIndex limit:(NSInteger)limit withCallBack:(requestCallBack)callBack;
- (void)addToCartListArtworkID:(NSString *)workID withCallBack:(requestCallBack)callBack;
- (void)deleteFromCartListArtworkID:(NSString *)workID withCallBack:(requestCallBack)callBack;
//Purchased
- (void)getPurchasedArtworksWithStartIndex:(NSInteger)start limit:(NSInteger)limit withCallBack:(requestCallBack)callBack;
- (void)purchasedArtwork:(NSString *)artworkID numberOfWorks:(NSString *)number withCallBack:(requestCallBack)callBack;
//News
- (void)getNewsWithStartIndex:(NSInteger)startIndex limit:(NSInteger)limit callBack:(requestCallBack)callBack;
//searche
- (void)searcheNewsWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack;
- (void)searcheAuthorWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack;
- (void)searcheCategoryWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack;
- (void)searcheArtworkWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack;
//Contact gallary
- (void)contactGalleryWithMessage:(NSString *)message callBack:(requestCallBack)callBack;

@end
