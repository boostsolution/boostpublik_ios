//
//  DownloadManager.m
//  ArtNewsApp
//
//  Created by iMac on 11.09.15.
//  Copyright © 2015 Boost. All rights reserved.
//

#import "DownloadsManager.h"
#import "User.h"
#import "Artworks.h"
#import "Result.h"
#import "Author.h"
#import "ArtCategory.h"
#import "News.h"
#import "AppDelegate.h"
#import "Result.h"

#define kTokenKey  @"profile"

static NSString *const baseURL = @"http://52.19.168.54:8080/api/V1/";


@interface DownloadsManager ()


@property (strong, nonatomic, readonly) NSString *token;
@property (strong, nonatomic, readonly) User *currentUser;

@end

@implementation DownloadsManager
#pragma mark - init and setup sctions
+ (instancetype)sharedInstance
{
    static DownloadsManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DownloadsManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    [RKObjectManager setSharedManager:objectManager];
    _token = [self getTokenIsExists];
}

#pragma mark - check loginning
- (BOOL)isAutentificate
{
    if (self.token.length)
        return YES;
    return NO;
}

#pragma mark - get current user
- (NSString *)currentUserID
{
    return self.currentUser.userID;
}

- (BOOL)isAuthor
{
    return YES;
}

- (void)setUserIcon:(UIImage *)icon
{
    NSData *iconData = UIImagePNGRepresentation(icon);
    [[NSUserDefaults standardUserDefaults] setObject:iconData forKey:self.currentUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (UIImage *)getUserIcon
{
    UIImage *result = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:self.currentUserID]];
    return result;
}

#pragma mark - save tocken
- (void)saveToken:(User *)user
{
    _token = user.token;
    _currentUser = user;
    [[NSUserDefaults standardUserDefaults] setObject:self.token forKey:kTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)logout
{
    // remove the accessToken
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _token = nil;
    _currentUser = nil;
}

- (NSString *)getTokenIsExists
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
}

#pragma mark - User metod
- (void)createNewUser:(User *)user callBack:(requestCallBack)callBack
{
    RKObjectMapping * userMapping = [User getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userMapping method:RKRequestMethodPOST pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@usercreate", baseURL];
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjects:@[user.mail, user.password] forKeys:@[@"email", @"password"]];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:jsonData requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            User *user = (User*) mappingResult.firstObject;
            [self saveToken:user];
            callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)updateUserWithData:(NSDictionary *)userData callBack:(requestCallBack)callBack
{
    RKObjectMapping * userMapping = [User getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userMapping method:RKRequestMethodPUT pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@userupdate/%@", baseURL, self.currentUserID];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userData options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:jsonData requestType:@"PUT" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}


- (void)autentificationWith:(NSString *)login password:(NSString *)password callBack:(requestCallBack)callBack
{
    RKObjectMapping *userMapping = [User getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@userauth", baseURL];
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:@{@"email": login,
                                                                                 @"password": password}];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:jsonData requestType:@"POST" needsSetHeaders:NO] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        User *user = (User*) mappingResult.firstObject;
        user.mail = login;
        [self saveToken:user];
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getUserInfoWithCallBack:(requestCallBack)callBack
{
    RKObjectMapping * userMapping = [User getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getuserinfo/%@", baseURL, self.currentUserID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)confirmUserEmail:(NSString *)email privateKey:(NSString *)key callBack:(requestCallBack)callBack
{
    
    RKResponseDescriptor *responseDescriptor = [Result getErrorDescriptorForMetod:RKRequestMethodPOST];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@confirmuseremail/%@", baseURL, email];
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:@{@"recoveryphrase": key}];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:jsonData requestType:@"POST" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)resetPasswordWithEmail:(NSString *)email callBack:(requestCallBack)callBack
{
    RKResponseDescriptor *responseDescriptor = [Result getErrorDescriptorForMetod:RKRequestMethodGET];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@userpasswordreset/%@", baseURL, email];
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)setNewPassword:(NSString *)password email:(NSString *)email recovery:(NSString *)recoveryKey callBack:(requestCallBack)callBack
{
    RKObjectMapping *userMapping = [User getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *loginUrl = [NSString stringWithFormat:@"%@userconfirmnewpassword/%@", baseURL, email];
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:@{@"recoveryphrase": recoveryKey,
                                                                                 @"password": password}];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:loginUrl bodyData:jsonData requestType:@"POST" needsSetHeaders:NO] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        User *user = (User*) mappingResult.firstObject;
        [self saveToken:user];
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:user]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Artworks screen
- (void)getArtworksWithStartIndex:(int)startIndex limit:(int)limit sortMode:(BOOL)mode withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    NSString *sortMode = mode ? @"getArtworkByLike" : @"getArtworkByDate";
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@%@/%d/%d", baseURL, sortMode, startIndex, limit];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getAllLikesArtworkStartIndex:(NSInteger)start limit:(NSInteger)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getLikesByUser/%@/%ld/%ld", baseURL, self.currentUserID, (long)start, (long)limit];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getArtworksWithAutor:(NSString *)author startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@getartworkbyauthor/%@/%d/%d", baseURL, author, startIndex, limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getArtworksWithCategory:(NSString *)category startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getartworkbycategory/%@/%d/%d", baseURL, category, startIndex, limit];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor, [Result getErrorDescriptorForMetod:RKRequestMethodGET]]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getArtworkWithID:(NSString *)artworkID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getartwork/%@", baseURL, artworkID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor, [Result getErrorDescriptorForMetod:RKRequestMethodGET]]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Author requests
- (void)getAuthorsWithMode:(BOOL)mode startIndex:(int)startIndex limit:(int)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * authorMapping = [Author getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:authorMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    NSString *sortMode = mode ? @"listAuthorsByAlphabet" : @"listAuthorsByFollowers";
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@%@/%d/%d", baseURL, sortMode, startIndex, limit];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)getAuthorsWithID:(NSString *)authorID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * authorMapping = [Author getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:authorMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@getAuthorDetails/%@", baseURL, authorID] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)becomeAuthorWithEmail:(NSString *)email fullName:(NSString *)fullName withCallBack:(requestCallBack)callBack
{
    RKResponseDescriptor *responseDescriptor = [Result getErrorDescriptorForMetod:RKRequestMethodGET];
    
    NSString *strUrlRequestAdress = [[NSString stringWithFormat:@"%@becomeauthor/%@/%@", baseURL, email, fullName] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)followAuthor:(NSString *)autorID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * authorMapping = [Author getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:authorMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@followAuthor/%@/%@", baseURL, self.currentUserID, autorID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)unfollowAuthor:(NSString *)autorID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * authorMapping = [Author getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:authorMapping
                                                                                            method:RKRequestMethodDELETE
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@unfollowAuthor/%@/%@", baseURL, self.currentUserID, autorID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"DELETE" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Category requests
- (void)getCategoryWithCallBack:(requestCallBack)callBack
{
    RKObjectMapping * categoryMapping = [ArtCategory getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:categoryMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@categorylist", baseURL];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Payment list
- (void)getPaymentsListWithCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getFavorite", baseURL];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Likes ArtWork
- (void)likeArtwork:(NSString *)workID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping *artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@likeArtwork/%@/%@", baseURL, self.currentUserID, workID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)unLikeArtwork:(NSString *)workID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping *artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodDELETE
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@unlikeArtwork/%@/%@", baseURL, self.currentUserID, workID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"DELETE" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Cart List
- (void)getCartListStartIndex:(NSInteger)startIndex limit:(NSInteger)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [NSString stringWithFormat:@"%@getInCart/%@/%ld/%ld", baseURL, self.currentUserID, (long)startIndex, (long)limit];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)addToCartListArtworkID:(NSString *)workID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping *artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@addToCart/%@/%@", baseURL, self.currentUserID, workID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor, [Result getErrorDescriptorForMetod:RKRequestMethodPOST]]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)deleteFromCartListArtworkID:(NSString *)workID withCallBack:(requestCallBack)callBack
{
    RKObjectMapping *artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodDELETE
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@removeFromCart/%@/%@", baseURL, self.currentUserID, workID];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"DELETE" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor, [Result getErrorDescriptorForMetod:RKRequestMethodDELETE]]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Purchased 
- (void)getPurchasedArtworksWithStartIndex:(NSInteger)start limit:(NSInteger)limit withCallBack:(requestCallBack)callBack
{
    RKObjectMapping * artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@getPurchased/%@/%ld/%ld", baseURL, self.currentUserID, (long)start, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];

}

- (void)purchasedArtwork:(NSString *)artworkID numberOfWorks:(NSString *)number withCallBack:(requestCallBack)callBack
{
    RKObjectMapping *artworkMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:artworkMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@purchaseArtwork/%@/%@/%@", baseURL, self.currentUserID, artworkID, number];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - NEWS
- (void)getNewsWithStartIndex:(NSInteger)startIndex limit:(NSInteger)limit callBack:(requestCallBack)callBack
{
    RKObjectMapping * newsMapping = [News getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:newsMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@getNews/%ld/%ld", baseURL, (long)startIndex, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - SEARCHE
- (void)searcheNewsWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack
{
    RKObjectMapping * newsMapping = [News getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:newsMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@searchNews/%@/%ld/%ld", baseURL, text, (long)index, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)searcheAuthorWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack
{
    RKObjectMapping * authorMapping = [Author getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:authorMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@searchAuthor/%@/%ld/%ld", baseURL, text, (long)index, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)searcheCategoryWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack
{
    RKObjectMapping * categoryMapping = [ArtCategory getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:categoryMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@searchCategoty/%@/%ld/%ld", baseURL, text, (long)index, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

- (void)searcheArtworkWithText:(NSString *)text currentPosition:(NSInteger)index limit:(NSInteger)limit callBack:(requestCallBack)callBack
{
    RKObjectMapping * workMapping = [Artworks getObjectMapping];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:workMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    NSString * strUrlRequestAdress = [[NSString stringWithFormat:@"%@searchArtwork/%@/%ld/%ld", baseURL, text, (long)index, (long)limit] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:nil requestType:@"GET" needsSetHeaders:YES] copy];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithArray:mappingResult.array]);});
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Contact gallery
- (void)contactGalleryWithMessage:(NSString *)message callBack:(requestCallBack)callBack
{
    NSString *strUrlRequestAdress = [NSString stringWithFormat:@"%@contactGallery/", baseURL];
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjects:@[message] forKeys:@[@"userMessage"]];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSURLRequest *request = [[self createUrlRequestWithUrlAddress:strUrlRequestAdress bodyData:jsonData requestType:@"POST" needsSetHeaders:YES] copy];
    
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request
                                                                                     responseDescriptors:@[[Result getSuccesDescriptorForMetod:RKRequestMethodPOST]]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithObject:mappingResult]);});
    } failure: ^(RKObjectRequestOperation * operaton, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{callBack([[RequestResult alloc] initWithError:error]);});
    }];
    
    [objectRequestOperation start];
}

#pragma mark - Create Request
- (NSURLRequest *)createUrlRequestWithUrlAddress:(NSString *)stringUrlAddress bodyData:(NSData *)bodyData requestType:(NSString *)requestType needsSetHeaders:(BOOL)headers{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSURL *urlAddress = [NSURL URLWithString:stringUrlAddress];
    [request setURL:urlAddress];
    [request setHTTPMethod:requestType];
    [request setHTTPBody:bodyData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"must-revalidate" forHTTPHeaderField:@"Cashe-Control"];
    [request setTimeoutInterval:30.0f];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    if ([requestType isEqualToString:@"PATCH"]){
        [request setValue:@"PATCH" forHTTPHeaderField:@"X-HTTP-Method-Override"];
    }
    if(headers){
        [request setValue:self.token forHTTPHeaderField:@"x-access-token"];
        [request setValue:self.currentUserID forHTTPHeaderField:@"userid"];
//        [request setValue:userName forHTTPHeaderField:@"X-Username"];
//        [request setValue:userPassword forHTTPHeaderField:@"X-Password"];
    }
    return request;
}

@end
