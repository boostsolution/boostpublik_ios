//
//  User.h
//  ArtNewsApp
//
//  Created by iMac on 14.09.15.
//  Copyright © 2015 Boost. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "InputImage.h"

@interface User : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *mail;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *postcode;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *suburb;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) InputImage *userIcon;
@property (strong, nonatomic) NSString *token;
@property (nonatomic) BOOL mailValid;

+ (RKObjectMapping *)getObjectMapping;
+ (NSDictionary *)getObjectMappingDictionary;

- (NSString *)userFullName;

@end
