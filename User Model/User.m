//
//  User.m
//  ArtNewsApp
//
//  Created by iMac on 14.09.15.
//  Copyright © 2015 Boost. All rights reserved.
//

#import "User.h"

@interface User ()

@end

@implementation User

@synthesize firstName, lastName, mail, password, street, city, state, postcode, country, userID, userIcon, token;


+ (RKObjectMapping *)getObjectMapping
{
    RKObjectMapping * userMapping = [RKObjectMapping mappingForClass:[User class]];
    [userMapping addAttributeMappingsFromDictionary:[User getObjectMappingDictionary]];
  
    return userMapping;
}

+ (NSDictionary *)getObjectMappingDictionary
{
    return @{@"first"         : @"firstName",
             @"last"          : @"lastName",
             @"email"         : @"mail",
             @"password"      : @"password",
             @"_id"           : @"userID",
             @"validemail"    : @"mailValid",
             @"token"         : @"token",
             @"country"       : @"country",
             @"postindex"     : @"postcode",
             @"state"         : @"state",
             @"street"        : @"street",
             @"suburb"        : @"suburb",
             @"city"          : @"city"};
}

- (NSString *)userFullName
{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"{\nFullName => %@ \nEmail => %@ \nPass => %@ \nStreat => %@ \nCity => %@ \nState => %@ \nZipCode => %@ \nCountry => %@ \nUserID => %@ \nIconURL => %@ \nIconURL => %@\n}", self.userFullName, self.mail, self.password, self.street, self.city, self.state, self.postcode, self.country, self.userID, self.userIcon.description, self.token];
}

@end
